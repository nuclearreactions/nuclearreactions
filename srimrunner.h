#ifndef SRIMRUNNER_H
#define SRIMRUNNER_H

#include <QObject>
#include <QProcess>
#include <QDebug>
#include<QDir>

class srimRunner : public QObject
{
    Q_OBJECT
public:
    explicit srimRunner(QObject *parent = 0);
    void setSRIMPath(QString srimPath_v){
        srimPath=srimPath_v;
    }
    void runSRIM();

    int returnExitStatus();

private:
    QString srimPath;
    QProcess *SRIM;
    
signals:
    
public slots:
    
};

#endif // SRIMRUNNER_H
//#ifdef linux
//    ...
//#elif _WIN32
//    ...
//#else
//    ...
//#endif
