#-------------------------------------------------
#
# Project created by QtCreator 2013-09-10T21:18:18
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = nuclearCalculationsOldSchool
TEMPLATE = app


SOURCES += main.cpp\
        nuclearrections.cpp \
    srimrunner.cpp

HEADERS  += nuclearrections.h \
    srimrunner.h

FORMS    += nuclearrections.ui

install_it.path = $$OUT_PWD\release
install_it.files += $$PWD\mass.mas03
install_it.files += $$PWD\elements.dat
INSTALLS += install_it

OTHER_FILES += \
    mass.mas03 \
    elements.dat
