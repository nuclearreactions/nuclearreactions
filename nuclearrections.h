#ifndef NUCLEARRECTIONS_H
#define NUCLEARRECTIONS_H

#include <QMainWindow>
#include <QFile>
#include <QMap>
#include <QTextStream>
#include <QDebug>
#include <QComboBox>
#include <math.h>
#include <QFileDialog>
#include <QSettings>
#include <srimrunner.h>

#define PI 3.14159265

namespace Ui {
class nuclearRections;
}

class nuclearRections : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit nuclearRections(QWidget *parent = 0);
    void setElementIsotopeToCB(QComboBox *element, const QString &arg1);
    void getUiValues();
    QString calculateQValue();
    QString calculateEnergeticsPlus();
    ~nuclearRections();
    
private slots:
    void on_targetCB_currentTextChanged(const QString &arg1);

    void on_beamCB_currentTextChanged(const QString &arg1);

    void on_evaporationCB_currentTextChanged(const QString &arg1);

    void on_productCB_currentTextChanged(const QString &arg1);

    void on_targetMNCB_currentTextChanged(const QString &arg1);

    void on_beamMNCB_currentTextChanged(const QString &arg1);

    void on_evaporationMNCB_currentTextChanged(const QString &arg1);

    void on_productMNCB_currentTextChanged(const QString &arg1);

    void on_beamEnergySB_valueChanged(const QString &arg1);

    void on_angleSB_valueChanged(const QString &arg1);

    void on_runSRIM_clicked();

    void on_writeSrim_clicked();

private:
    Ui::nuclearRections *ui;
    //Read in the mass data file
    void readAmeData(){
        QFile file("mass.mas03");
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return;
        QTextStream in(&file);
        int lineNo=0;
        while (!in.atEnd()) {
            QString line = in.readLine();
            if(lineNo>=39){
                parseAmeData(line);
            }
            lineNo++;
        }
        file.close();
    }

    void parseAmeData(QString line){
        if(line.at(0)==QChar('0')){
            line.remove(0,1);
        }
        line.simplified();
        QStringList splitLine=line.split(" ",QString::SkipEmptyParts);
        if(splitLine.contains("*")){
            int indexOfStar=splitLine.indexOf("*");
            splitLine.insert(indexOfStar+1,"-");
        }
        if(splitLine.length()==15){
            splitLine.insert(4+1,"Stable");
        }
        ameData.append(splitLine);
        //Extract masses
        QString protonNumber=splitLine.at(2);
        QString massNumber=splitLine.at(3);
        QString element=splitLine.at(4);
        QString massString0=splitLine.at(13); //The masses are put annoyingly as 1 xxxxx, meaning there is a space between the ~A and the decimals
        QString massString=splitLine.at(14);
        massString.remove(QChar('#'));
        QString finalMassString=massString0+massString;
        double mass=finalMassString.toDouble();
        QString key=massNumber+element;
        ameMasses.insert(key, mass);
        if(elementIsotopes.contains(element)){
            QStringList currentElement=elementIsotopes.value(element);
            currentElement.append(massNumber);
            elementIsotopes.insert(element,currentElement );
        }
        else{
            QStringList currentElement;
            currentElement.append(massNumber);
            elementIsotopes.insert(element,currentElement );
        }
    }

    void readElementData(){
        QFile file("elements.dat");
            if (!file.open(QIODevice::ReadOnly | QIODevice::Text)){
                return;
            }
            QTextStream in(&file);
            while (!in.atEnd()) {
                QString line = in.readLine();
                parseElementData(line);
            }
            file.close();
    }
    void parseElementData(QString line){
        QStringList lineSplit=line.split("\t", QString::SkipEmptyParts);
        elementData.append(lineSplit);
        QString Z=lineSplit.at(0);
        QString element=lineSplit.at(1);
        elementNames.insert(element, Z);
    }

    void setResultsToUi(QString info="", QString result="");


    //key is  massNumber_element_protonNumber
    QList<QStringList> ameData;
    QHash<QString, double> ameMasses;
    QList<QStringList> elementData;
    QHash<QString, QString> elementNames;
    QHash<QString, QStringList> elementIsotopes;
    double c; //Speed of light
    double u; //atomic mas unit
    double mb;  //Evaporation
    double my;  //Product
    double mx;  //Target
    double ma;  //Beam
    double beamEnergy;
    double evaporationAngle;

    QString mbS;  //Evaporation
    QString myS;  //Product
    QString mxS;  //Target
    QString maS;  //Beam

    double QValue;
    double Tb;
    double compoundNucleusEnergy;


    //SRIM related stuff
    srimRunner *srim;
    QString srimDir;
    QFile srimModule;
    QHash<QString, QStringList> srimAtomicData;
    QStringList srimInputFile;
    QStringList srimOutputFile;
    QString outputFilename;
    void srimDirSelector();
    void openSrimAtomicData();
    void openSrimIn();
    void modifySrimIn();
    //sub routines fro modifying various parts of SR.IN
    void modifySrimInTarget();
    void writeSrimIn();
    void openSrimOutput();
};

#endif // NUCLEARRECTIONS_H
