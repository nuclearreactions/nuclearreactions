#include "srimrunner.h"

srimRunner::srimRunner(QObject *parent) :
    QObject(parent)
{
    SRIM = new QProcess(this);
}

void srimRunner::runSRIM(){
    if(SRIM->exitStatus()==0){
#ifdef linux
    SRIM->setProgram(srimPath+"/SRModule.exe");
    SRIM->setWorkingDirectory(srimPath);
    SRIM->start();
#elif _WIN32
    SRIM->setProgram(srimPath+"/SRModule.exe");
    SRIM->setWorkingDirectory(srimPath);
    SRIM->start();
#else
    ...
#endif
}

}

int srimRunner::returnExitStatus(){
    return SRIM->exitStatus();
}
