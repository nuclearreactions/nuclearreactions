#include "nuclearrections.h"
#include "ui_nuclearrections.h"

nuclearRections::nuclearRections(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::nuclearRections)
{
    readElementData();
    readAmeData();
    QStringList xneutrons;
    xneutrons<<"1"<<"2"<<"3"<<"4"<<"5"<<"6"<<"7"<<"8"<<"9"<<"10";
    elementIsotopes.insert("n", xneutrons);
    c=299792458;       //[m/s]
    u= 931.494061;  //µ[MeV/c^2] in µ-U's as masses in ame database are in µ-u
    ui->setupUi(this);
    //add neutron
    ui->evaporationCB->addItem("n");
    foreach(QString key, elementNames.keys()){
        ui->targetCB->addItem(key);
        ui->beamCB->addItem(key);
        ui->evaporationCB->addItem(key);
        ui->productCB->addItem(key);

        ui->targetCB->setCurrentText("Ni");
        ui->beamCB->setCurrentText("H");
        ui->evaporationCB->setCurrentText("n");
        ui->productCB->setCurrentText("Cu");
    }
    ui->resultBox->clear();

    //SRim runner
    srim = new srimRunner(this);
    srimDirSelector();
    ui->runSRIM->setDisabled(true);
    ui->writeSrim->setDisabled(true);
    if(!srimDir.isEmpty() && srimModule.exists()){
        srim->setSRIMPath(srimDir);
        openSrimAtomicData();
        openSrimIn();
        ui->runSRIM->setEnabled(true);
        ui->writeSrim->setEnabled(true);
    }
}

nuclearRections::~nuclearRections()
{
    delete ui;
}

void nuclearRections::setElementIsotopeToCB(QComboBox *element, const QString &arg1){
    QStringList isotopes=elementIsotopes.value(arg1);
    element->clear();
    element->addItems(isotopes);
}
void nuclearRections::getUiValues(){
    mbS=ui->evaporationMNCB->currentText()+ui->evaporationCB->currentText();
    myS=ui->productMNCB->currentText()+ui->productCB->currentText();
    mxS=ui->targetMNCB->currentText()+ui->targetCB->currentText();
    maS=ui->beamMNCB->currentText()+ui->beamCB->currentText();

    mb=ameMasses.value(mbS);
    my=ameMasses.value(myS);
    mx=ameMasses.value(mxS);
    ma=ameMasses.value(maS);
    if(ui->evaporationCB->currentText()=="n"){
        QString xnS=ui->evaporationMNCB->currentText();
        double xn=ui->evaporationMNCB->currentText().toDouble();
        mb=ameMasses.value("1n")*xn;
    }

    beamEnergy=ui->beamEnergySB->value();
    evaporationAngle=ui->angleSB->value();

}


//following notation given in Krane
QString nuclearRections::calculateQValue(){
    getUiValues();
    QValue=(mx+ma-my-mb)*u/1E6;
    return "Q-Value="+QString::number(QValue)+" MeV";
}

QString nuclearRections::calculateEnergeticsPlus(){
    double Ta=beamEnergy;
    double cosphi=cos(evaporationAngle* PI / 180.0);
    double sqrtTb=(sqrt(ma*mb*Ta)*cosphi+sqrt(ma*mb*Ta*pow(cosphi,2)+(my+mb)*(my*QValue+(my-ma)*Ta)))/(my+mb);
    Tb=pow(sqrtTb,2);
    double Ty=QValue+Ta-Tb;
    compoundNucleusEnergy=Ta*ma/(ma+mx);
    return "Tb="+QString::number(Tb)+" MeV"+" Ty="+QString::number(Ty)+" MeV"+", compound nucleus energy= "+QString::number(compoundNucleusEnergy)+" MeV";
}

void nuclearRections::setResultsToUi(QString info, QString result){
    QString infoNull=info;
    ui->resultBox->append(result);
}

//SRIM Module related code
void nuclearRections::srimDirSelector(){
    QSettings settings(QDir::currentPath()+"/nuclearReactions.dat",QSettings::IniFormat);
    srimDir=settings.value("path").toString();
    srimModule.setFileName(srimDir +"/SRModule.exe");
    if(srimDir.isEmpty()|| !srimModule.exists()){
        srimDir=QFileDialog::getExistingDirectory(this, tr("Select SR Module Directory"),QDir::homePath(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
        settings.setValue("path", srimDir);
    }
    srimModule.setFileName(srimDir +"/SRModule.exe");
}

void nuclearRections::openSrimAtomicData(){
    QString srimDataPath=srimDir;
    srimDataPath.remove("SR Module");
    srimDataPath.append("Data/ATOMDATA");
    qDebug()<<srimDataPath;
    QFile file(srimDataPath);
    if(file.exists()){
        srimAtomicData.clear();
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text)){
            return;
        }
        QTextStream in(&file);
        while (!in.atEnd()) {
            QString line = in.readLine().simplified();
            QStringList lineSplit=line.split(" ");
            QString element=lineSplit.at(1);
            srimAtomicData.insert(element, lineSplit);
        }
        file.close();
    }
}

void nuclearRections::openSrimIn(){
    QFile srimIn;
    srimInputFile.clear();
    srimIn.setFileName(srimDir +"/SR.IN");
    if(srimIn.exists()){
        ui->SrimInput->clear();
        if (!srimIn.open(QIODevice::ReadOnly | QIODevice::Text))
            return;
        QTextStream in(&srimIn);
        while(!in.atEnd()){
            QString line=in.readLine();
            srimInputFile.append(line);
            ui->SrimInput->appendPlainText(line);
        }
    }
}
void nuclearRections::modifySrimIn(){
    QString target=ui->targetCB->currentText();

    if(srimAtomicData.contains(target)){
        bool forWhich=ui->forWhich->isChecked();

        //Modify the mass of the ion
        int indexofIon=srimInputFile.indexOf("---Ion(Z), Ion Mass(u)");
        QString Z;
        QString mass;
        getUiValues();
        if(!forWhich){
            Z=elementNames.value(ui->beamCB->currentText());
            mass=QString::number(ma/1E6);
        }
        else{
            Z=elementNames.value(ui->productCB->currentText());
            mass=QString::number(my/1E6);
        }
        QString newIon=Z+"   "+mass;
        srimInputFile.replace(indexofIon+1, newIon);
        //modify target, so far only pure elements
        modifySrimInTarget();
        //modify ion energy
        int indexOfIonEnergy=srimInputFile.indexOf("---Ion Energy : E-Min(keV), E-Max(keV)");
        QString lowerEnergy;
        QString upperEnergy;
        if(!forWhich){
            lowerEnergy=QString::number(ui->beamEnergySB->value()*1000);
        }
        else{
            lowerEnergy=QString::number(compoundNucleusEnergy*1000);
        }
        upperEnergy=lowerEnergy;
        QString newIonEnergy=lowerEnergy + "    " + upperEnergy;
        srimInputFile.replace(indexOfIonEnergy+1, newIonEnergy);
        //input to ui and write SR.IN
        ui->SrimInput->clear();
        foreach (QString line, srimInputFile) {
            ui->SrimInput->appendPlainText(line);
        }
        QFile SRIN;
        srimOutputFile.clear();
        SRIN.setFileName(srimDir +"/SR.IN");
        SRIN.remove();
        if(!SRIN.exists()){
            if (!SRIN.open(QIODevice::ReadWrite | QIODevice::Text))
                return;
            QTextStream writeSRIN(&SRIN);
            qDebug()<<srimInputFile;
            foreach (QString line, srimInputFile) {
                writeSRIN<<line<<"\r\n";  // \r\n is to maintain windows end-of-line, otherwise SRIMS does not work
            }

        }
        SRIN.close();
    }
}
void nuclearRections::modifySrimInTarget(){
    bool gasTarget=ui->gasTarget->isChecked();
    int isGas=0;
    int densityIndex=0;
    if(gasTarget){
        isGas=1;
        densityIndex=10;
    }
    else {
        isGas=0;
        densityIndex=6;
    }
    QString target=ui->targetCB->currentText();
    QStringList srimTargetElement=srimAtomicData.value(target);
    qDebug()<<srimTargetElement;
    QString targetDensity=srimTargetElement.at(densityIndex);

    //Change the target data
    int indexOfTargetData=srimInputFile.indexOf("---Target Data: (Solid=0,Gas=1), Density(g/cm3), Compound Corr.");
    QString newTargetData=QString::number(isGas)+"    "+targetDensity+"    "+"1";
    srimInputFile.replace(indexOfTargetData+1, newTargetData);
    //Change the number of target elements
    int indexOfNumberOfTargetElements=srimInputFile.indexOf("---Number of Target Elements");
    srimInputFile.replace(indexOfNumberOfTargetElements+1,"1");
    //Change the target
    int beginTargetIndex=srimInputFile.indexOf("---Target Elements: (Z), Target name, Stoich, Target Mass(u)");
    int endTargetIndex=srimInputFile.indexOf("---Output Stopping Units (1-8)");
    for(int i=endTargetIndex-1; i>beginTargetIndex; i--){
        srimInputFile.removeAt(i);
    }
    QString Z;
    QString mass;
    QString stoich="1";
    Z=elementNames.value(ui->targetCB->currentText());
    mass=QString::number(mx/1E6);
    QString newTarget=Z+"   "+'\"'+ui->targetCB->currentText()+'\"'+"               "+stoich+"             "+mass;
    srimInputFile.insert(beginTargetIndex+1, newTarget);

}


void nuclearRections::openSrimOutput(){
    outputFilename;
    int indexOfOutputfileName=srimInputFile.indexOf("---Output File Name");
    outputFilename=srimInputFile.at(indexOfOutputfileName+1);
    outputFilename.remove('\"');
    QFile srimOut;
    srimOutputFile.clear();
    srimOut.setFileName(srimDir +"/"+outputFilename);
    if(srimOut.exists()){
        ui->SrimOutput->clear();
        if (!srimOut.open(QIODevice::ReadOnly | QIODevice::Text))
            return;
        QTextStream out(&srimOut);
        while(!out.atEnd()){
            QString line=out.readLine();
            srimOutputFile.append(line);
            ui->SrimOutput->appendPlainText(line);
        }
    }

}

//Slots
void nuclearRections::on_targetCB_currentTextChanged(const QString &arg1)
{
    setElementIsotopeToCB(ui->targetMNCB, arg1);
    setResultsToUi("Q-value", calculateQValue());
    setResultsToUi("Energetics", calculateEnergeticsPlus());
}

void nuclearRections::on_beamCB_currentTextChanged(const QString &arg1)
{
    setElementIsotopeToCB(ui->beamMNCB, arg1);
    setResultsToUi("Q-value", calculateQValue());
    setResultsToUi("Energetics", calculateEnergeticsPlus());
}

void nuclearRections::on_evaporationCB_currentTextChanged(const QString &arg1)
{
    setElementIsotopeToCB(ui->evaporationMNCB, arg1);
    setResultsToUi("Q-value", calculateQValue());
    setResultsToUi("Energetics", calculateEnergeticsPlus());

    if(arg1=="n"){
        ui->evaporationElement->setText("Neutron");
        ui->evaporationIsotope->setText("No. of n's");
    }
    else{
        ui->evaporationElement->setText("Element");
        ui->evaporationIsotope->setText("Isotope");
    }
}

void nuclearRections::on_productCB_currentTextChanged(const QString &arg1)
{
    setElementIsotopeToCB(ui->productMNCB, arg1);
    setResultsToUi("Q-value", calculateQValue());
    setResultsToUi("Energetics", calculateEnergeticsPlus());
}

void nuclearRections::on_targetMNCB_currentTextChanged(const QString &arg1)
{
    QString argNull=arg1;
    argNull="0";
    setResultsToUi("Q-value", calculateQValue());
    setResultsToUi("Energetics", calculateEnergeticsPlus());
}

void nuclearRections::on_beamMNCB_currentTextChanged(const QString &arg1)
{
    QString argNull=arg1;
    argNull="0";
    setResultsToUi("Q-value", calculateQValue());
    setResultsToUi("Energetics", calculateEnergeticsPlus());
}

void nuclearRections::on_evaporationMNCB_currentTextChanged(const QString &arg1)
{
    QString argNull=arg1;
    argNull="0";
    setResultsToUi("Q-value", calculateQValue());
    setResultsToUi("Energetics", calculateEnergeticsPlus());
}

void nuclearRections::on_productMNCB_currentTextChanged(const QString &arg1)
{
    QString argNull=arg1;
    argNull="0";
    setResultsToUi("Q-value", calculateQValue());
    setResultsToUi("Energetics", calculateEnergeticsPlus());
}

void nuclearRections::on_beamEnergySB_valueChanged(const QString &arg1)
{
    QString argNull=arg1;
    argNull="0";
    setResultsToUi("Q-value", calculateQValue());
    setResultsToUi("Energetics", calculateEnergeticsPlus());
}

void nuclearRections::on_angleSB_valueChanged(const QString &arg1)
{
    QString argNull=arg1;
    argNull="0";
    setResultsToUi("Q-value", calculateQValue());
    setResultsToUi("Energetics", calculateEnergeticsPlus());
}

void nuclearRections::on_runSRIM_clicked()
{
    srim->runSRIM();
    ui->SrimOutput->clear();
    openSrimOutput();
}

void nuclearRections::on_writeSrim_clicked()
{
    modifySrimIn();
}
